# Chimera - duplicate into a comment

A set of keymaps to duplicate and comment given lines in one go.

Useful for rephrasing a piece of text while keeping an eye on the (commented) original (chimera).

## compatible comment plugins

will be used if it's available (loaded)

- [Comment.nvim](github.com/numToStr/Comment.nvim)
- [vim-commentary](github.com/tpope/vim-commentary)

## features

- supports motions and text-objects
- visual-mode
- dot-repeatable

## installation

Clone into your `&runtimepath` or use your favorite plugin manager.

- [lazy.nvim](https://github.com/folke/lazy.nvim)

```lua
  {
    "https://gitlab.com/Biggybi/nvim-chimera.git",
    dependencies = {
      -- pick one or none
      "tpope/vim-commentary"
      "numToStr/Comment.nvim",
    },
    opts = {},
  },
```

## config

```lua default config
  {
    -- set to 'false' to disable all
    keymaps = {
      normal_line = "yc",
      visual_line = "gyc",
      normal_block = "yC",
      visual_block = "gyC",
    },
  }
```

## usage

- `yc{motion}`: paste a commented version of lines in _{motion}_ above selected lines
- `ycc`: paste a commented version of the current line above the cursor
