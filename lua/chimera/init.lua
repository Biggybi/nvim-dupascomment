local M = {}

---@param opts Chimera.Opts
M.setup = function(opts)
	require("chimera.config").setup(opts)
	require("chimera.chimera").create_keymaps()
end

return M
