local M = {}

local options = require("chimera.config").options
local utils = require("chimera.utils")

function M.chimera_default()
	vim.cmd("'[,']yank")
	vim.cmd("'[put!")
	vim.cmd("'[,']norm gcc")
end

function M.chimera_commentary()
	vim.cmd("'[,']yank")
	vim.cmd("'[put!")
	vim.cmd("'[,']Commentary")
end

function M.chimera_comment_nvim_block()
	vim.cmd("'[,']yank")
	vim.cmd("'[put!")
	vim.cmd("'[,']lua require('Comment.api').toggle.linewise()")
end

function M.chimera_comment_nvim_line()
	vim.cmd("'[,']yank")
	vim.cmd("'[put!")
	vim.cmd("'[,']lua require('Comment.api').toggle.linewise()")
end

function M.chimera()
	vim.cmd("'[,']yank")
	vim.cmd("'[put!")
	vim.cmd(M.comment_cmd)
end

local comment_cmds = {
	comment_nvim = {
		block = "'[,']lua require('Comment.api').toggle.blockwise(vim.fn.visualmode())",
		line = "'[,']lua require('Comment.api').toggle.linewise(vim.fn.visualmode())",
	},
	commentary = "'[,']Commentary",
	default = "'[,']norm gcc",
}

---@param type? "line" | "block"
function M.set_operator(type)
	M.comment_cmd = comment_cmds.default
	type = type or "line"
	if options.comment_plugin then
		if utils.loaded_comment_nvim() then
			M.comment_cmd = comment_cmds.comment_nvim[type]
		elseif utils.loaded_commentary() then
			M.comment_cmd = comment_cmds.commentary
		end
	end
	vim.o.operatorfunc = "v:lua.require'chimera.chimera'.chimera"
end

function M.operator_linewise()
	M.set_operator("line")
	return "g@"
end

function M.operator_linewise_line()
	M.set_operator("line")
	return "g@_"
end

function M.operator_blockwise()
	M.set_operator("block")
	return "g@"
end

function M.operator_blockwise_line()
	M.set_operator("block")
	return "g@_"
end

function M.create_keymaps()
	if not options or not options.keymaps then
		return
	end

	local map = vim.keymap.set

	map({ "n", "x" }, options.keymaps.normal_line, M.operator_linewise, { expr = true, desc = "Chimera" })
	map({ "n", "x" }, options.keymaps.normal_block, M.operator_blockwise, { expr = true, desc = "Chimera block" })

	map(
		"n",
		utils.line_keymap_lhs(options.keymaps.normal_line),
		M.operator_linewise_line,
		{ expr = true, desc = "Chimera line" }
	)
	map(
		"n",
		utils.line_keymap_lhs(options.keymaps.normal_block),
		M.operator_blockwise_line,
		{ expr = true, desc = "Chimera block line" }
	)
end

return M
