local M = {}

---@class Chimera.Keymaps
---@field normal_line? string
---@field visual_line? string
---@field normal_block? string
---@field visual_block? string

---@class Chimera.Opts
---@field create_keymaps? boolean
---@field keymaps? Chimera.Keymaps
---@field comment_plugin? boolean

---@type Chimera.Opts
local defaults = {
	keymaps = {
		normal_line = "yc",
		normal_block = "yC",
		visual_line = "gyc",
		visual_block = "gyC",
	},
	comment_plugin = true,
}

function M.setup(opts)
	---@type Chimera.Opts
	M.options = vim.tbl_deep_extend("force", {}, defaults, opts)
end

return M
