local M = {}

function M.loaded_commentary()
	return vim.g.loaded_commentary and vim.g.loaded_commentary == 1
end

function M.loaded_comment_nvim()
	return package.loaded["Comment.utils"] ~= nil
end

return M
